//
//  ARViewComponent.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 15/12/2021.
//

import ARKit
import SwiftUI
import RealityKit

struct BirbARViewContainer: UIViewRepresentable {

    @Binding var showComments: Bool
    let arView = ARView(frame: .zero)
    
    func makeUIView(context: Context) -> ARView {
        let anchor = try! Experience.loadBirdPrompt()
        anchor.actions.tapped.onAction = handleTapOnEntity(_:)
        
        let textEntity: Entity = anchor.textModel!.children[0].children[0]
        var textModelComponent: ModelComponent = (textEntity.components[ModelComponent.self])!
        
        var material = SimpleMaterial()
        material.baseColor = .color(.black)
        textModelComponent.materials[0] = material

        textModelComponent.mesh = .generateText("What usually brings you to peace?",
                                                extrusionDepth: 0.001,
                                                font: .systemFont(ofSize: 0.04),
                                                containerFrame: CGRect.zero,
                                                alignment: CoreText.CTTextAlignment.center,
                                                lineBreakMode: .byWordWrapping)
        anchor.textModel!.children[0].children[0].components.set(textModelComponent)        
        
        arView.scene.anchors.append(anchor)
        
        return arView
        
    }
    
    func handleTapOnEntity(_ entity: Entity?) {
        guard let entity = entity else { return }
        print("tapped")
        showComments = true
    }
  
    func updateUIView(_ uiView: ARView, context: Context) {}

}
