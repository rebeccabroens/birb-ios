//
//  ARView.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 10/12/2021.
//

import ARKit
import SwiftUI
import RealityKit

struct BirbARView: View {
    @State private var showComments = false
    var body: some View {
        ZStack {
            BirbARViewContainer(showComments: $showComments).ignoresSafeArea()
            CommentView(isShowing: $showComments)
        }
    }
}

struct BirbARView_Previews: PreviewProvider {
    static var previews: some View {
        BirbARView()
    }
}
