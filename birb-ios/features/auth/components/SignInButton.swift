//
//  SignInButton.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 17/12/2021.
//

import SwiftUI

struct SignInButton: View {
    var action: () -> Void
    var text: String
    var backgroundColor: Color
    var foregroundColor: Color
    
    var body: some View {
        Button(action: action) {
            Text(text)
        }
        .padding(.horizontal, 35)
        .padding(.vertical, 15)
        .background(backgroundColor)
        .foregroundColor(foregroundColor)
        .clipShape(Capsule())
    }
}

struct SignInButton_Previews: PreviewProvider {
    static var previews: some View {
        SignInButton(action: {}, text: "Sign in", backgroundColor: Color("Yellow"), foregroundColor: Color(.white)).previewLayout(.sizeThatFits)
    }
}
