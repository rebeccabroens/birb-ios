//
//  SignInView.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 17/12/2021.
//

import SwiftUI

struct SignInView: View {
    @State var username: String = ""
    @State var password: String = ""
    
    @EnvironmentObject var model: AppStateModel
    
    var body: some View {
            VStack {
                Spacer()
                Image("WelcomeSVG")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding()
                Text("Welcome to Birb!")
                    .font(.system(.largeTitle).bold())
                    .padding(5)
                Text("Inspire others by sharing your thoughts and feelings to the flock.")
                    .font(.system(.body))
                    .foregroundColor(Color("Text"))
                    .multilineTextAlignment(.center)
                VStack {
                    TextField("Username", text: $username)
                        .modifier(CustomTextField())
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .frame(width: 300)
                    SecureField("Password", text: $password)
                        .modifier(CustomTextField())
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .frame(width: 300)
                    
                    Button(action: {
                        SignIn()
                    }, label: {
                        Text("Sign In")
                            .frame(width: 220, height: 50)
                            .background(Color("LightYellow"))
                            .foregroundColor(Color("Yellow"))
                            .cornerRadius(7)
                    })
                }
                .padding(20)
                HStack {
                    NavigationLink("New to Birb? Create Account", destination: SignUpView())
                }
            }
    }
    
    func SignIn() {
        guard !username.trimmingCharacters(in: .whitespaces).isEmpty,
              !password.trimmingCharacters(in: .whitespaces).isEmpty,
              password.count >= 6 else {
                  return
              }
        
        model.signIn(username: username, password: password)
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
