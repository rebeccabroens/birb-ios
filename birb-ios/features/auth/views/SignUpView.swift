//
//  SignInView.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 17/12/2021.
//

import SwiftUI

struct SignUpView: View {
    @State var email: String = ""
    @State var username: String = ""
    @State var password: String = ""
    
    @EnvironmentObject var model: AppStateModel
    
    var body: some View {
            VStack {
                Text("Create your account!")
                    .font(.system(.largeTitle).bold())
                    .padding(5)
                VStack {
                    TextField("Email", text: $email)
                        .modifier(CustomTextField())
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .frame(width: 300)
                    TextField("Username", text: $username)
                        .modifier(CustomTextField())
                        .disableAutocorrection(true)
                        .frame(width: 300)
                    SecureField("Password", text: $password)
                        .modifier(CustomTextField())
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .frame(width: 300)
                    
                    Button(action: {
                        SignUp()
                    }, label: {
                        Text("Sign Up")
                            .frame(width: 220, height: 50)
                            .background(Color("LightYellow"))
                            .foregroundColor(Color("Yellow"))
                            .cornerRadius(7)
                    })
                }
                .padding(20)
                Spacer()
            }
            .navigationBarTitle("Create Account", displayMode: .inline)
    }
    
    func SignUp() {
        guard !email.trimmingCharacters(in: .whitespaces).isEmpty,
              !username.trimmingCharacters(in: .whitespaces).isEmpty,
              !password.trimmingCharacters(in: .whitespaces).isEmpty,
              password.count >= 6 else {
                  return
              }
        model.signUp(email: email, username: username, password: password)
        
    }
}
struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}
