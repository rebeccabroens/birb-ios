//
//  ChatRow.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import SwiftUI

struct ChatRow: View {
    let text: String
    let type: MessageType
    let image: String
    var isSender: Bool {
        return type == .sent
    }
    
    init(text: String, type: MessageType, image: String) {
        self.text = text
        self.type = type
        self.image = image
    }
    
    var body: some View {
        HStack {
            if isSender { Spacer () }
            
            if !isSender {
                VStack {
                    Spacer()
                    AsyncImage(url: URL(string: image)) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 60, height: 60)
                    .cornerRadius(15)
                }
            }
            HStack {
                Text(text)
                    .padding()
            }
            .foregroundColor(isSender ? Color.white : Color(.label))
            .background(isSender ? Color("Yellow") : Color(.systemGray4))
            .cornerRadius(6)
            .padding(isSender ? .leading : .trailing, isSender ? UIScreen.main.bounds.width/3 : UIScreen.main.bounds.width/5)
            if !isSender { Spacer() }
        }
        
    }
}

struct ChatRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ChatRow(text: "Hello World!", type: .sent, image: "https://preview.redd.it/15c512ny99n11.jpg?auto=webp&s=7f8272fd67f272001a5574c566b96240f9315e5a")
            ChatRow(text: "Oh hi there!", type: .received, image: "https://preview.redd.it/15c512ny99n11.jpg?auto=webp&s=7f8272fd67f272001a5574c566b96240f9315e5a")
        }
    }
}
