//
//  SendButton.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import SwiftUI

struct SendButton: View {
    @Binding var text: String
    @EnvironmentObject var model: AppStateModel
    
    var body: some View {
        Button(action: {
            self.sendMessage()
        }) {
            Image(systemName: "location.fill")
                .font(.system(size: 30))
                .aspectRatio(contentMode: .fit)
                .frame(width: 50, height: 50)
                .foregroundColor(Color("Text"))
                .background(Color("Yellow"))
                .clipShape(Circle())
        }
    }
    
    func sendMessage() {
        guard !text.trimmingCharacters(in: .whitespaces).isEmpty else {
            return
        }
        
        model.sendMessage(text: text)
        text = ""
    }
}

struct SendButton_Previews: PreviewProvider {
    static var previews: some View {
        SendButton(text: .constant("Hello World"))
    }
}
