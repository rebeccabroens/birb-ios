//
//  Message.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import Foundation

enum MessageType: String {
    case sent
    case received
}

struct Message: Hashable {
    let text: String
    let type: MessageType
    let created: Date
}
