//
//  User.swift
//  birb-ios
//
//  Created by Michelle Broens on 12/01/2022.
//

import Foundation

struct User: Hashable {
    let name: String
    let image: String
}
