//
//  ChatView.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import SwiftUI

struct ChatView: View {
    @State var message: String = ""
    let otherUser: String
    let image: String
    
    @EnvironmentObject var model: AppStateModel
    
    init(otherUser: String, image: String) {
        self.otherUser = otherUser
        self.image = image
    }
    
    var body: some View {
        VStack {
            ScrollView(.vertical) {
                ForEach(model.messages, id: \.self) { message in
                    ChatRow(text: message.text, type: message.type, image: image)
                        .padding(3)
                }
            }
            
            HStack {
                TextField("Message...", text: $message)
                    .modifier(CustomTextField())
                
                SendButton(text: $message)
            }
            .padding()
            .padding(.bottom, 20)
            .navigationBarTitle(otherUser, displayMode: .inline)
            .onAppear {
                model.currentOtherUser = otherUser
                model.observeChat()
            }
        }
    }
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        ChatView(otherUser: "Teddy", image: "https://preview.redd.it/15c512ny99n11.jpg?auto=webp&s=7f8272fd67f272001a5574c566b96240f9315e5a")
    }
}
