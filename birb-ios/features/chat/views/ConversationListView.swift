//
//  ConversationListView.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import SwiftUI

struct ConversationListView: View {
    
    @State var otherUser: Conversation = Conversation(name: "", image: "")
    @State var showChat  = false
    @State var showNewChat = false
    
    @EnvironmentObject var model: AppStateModel
    
    var body: some View {
            ScrollView(.vertical) {
                ForEach(model.conversationsTest, id: \.self) { conversation in
                    NavigationLink(destination: ChatView(otherUser: conversation.name, image: conversation.image), label: {
                        HStack {
                            AsyncImage(url: URL(string: conversation.image)) { image in
                                image.resizable()
                            } placeholder: {
                                ProgressView()
                            }
                            .frame(width: 60, height: 60)
                            .cornerRadius(15)
                            Text(conversation.name)
                                .bold()
                                .foregroundColor(Color(.label))
                                .font(.system(size: 24))
                            Spacer()
                        }
                        .padding()
                    })
                }
                
                if !otherUser.name.isEmpty {
                    NavigationLink("", destination: ChatView(otherUser: otherUser.name, image: otherUser.image), isActive: $showChat)
                }
            }
            .navigationTitle("Conversations")
            .toolbar {
                ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading){
                    Button("Sign Out") {
                        self.signOut()
                    }
                }
                
            }
            .toolbar {
                ToolbarItem(placement: ToolbarItemPlacement.navigationBarTrailing){
                    NavigationLink(
                        destination: NewChatView { conversation in
                            self.showNewChat = false
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                self.otherUser = Conversation(name: conversation.name, image: conversation.image)
                                self.showChat = true
                            }
                        },
                        isActive: $showNewChat,
                        label: {
                            Image(systemName: "square.and.pencil")
                        })
                }
            }
            .onAppear {
                guard model.auth.currentUser != nil else {
                    return
                }
                model.getConversations()
                model.getConversationsTest()
            }
    }
    
    func signOut() {
        model.signOut()
    }
}

struct ConversationListView_Previews: PreviewProvider {
    static var previews: some View {
        ConversationListView()
    }
}
