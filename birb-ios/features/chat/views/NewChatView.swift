//
//  SearchView.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import SwiftUI

struct NewChatView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var text: String = ""
    @State var users: [User] = []
    
    let completion: ((User) -> Void)
    
    @EnvironmentObject var model: AppStateModel
    
    init(completion: @escaping ((User) -> Void)) {
        self.completion = completion
    }
    
    var body: some View {
        VStack {
            TextField("Username...", text: $text)
                .modifier(CustomTextField())
            
            Button("Search") {
                guard !text.trimmingCharacters(in: .whitespaces).isEmpty else {
                    return
                }
                model.searchUsers(queryText: text) { users in
                    self.users = users
                }
            }
            
            List {
                ForEach(users, id: \.self) { user in
                    HStack {
                        // add image
                        Circle()
                            .frame(width: 40, height: 40)
                            .foregroundColor(Color.pink)
                        
                        Text(user.name)
                            .font(.title3)
                        Spacer()
                    }
                    .onTapGesture {
                        presentationMode.wrappedValue.dismiss()
                        completion(user)
                    }
                }
            }
            
            Spacer()
        }
        .navigationTitle("Start a new chat")    }
}

struct NewChatView_Previews: PreviewProvider  {
    static var previews: some View {
        NewChatView() { _ in }
    }
}
