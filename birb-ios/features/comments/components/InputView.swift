//
//  InputView.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import SwiftUI

struct CommentInputView: View {
    @State private var value: String = ""
    
    var body: some View {
            HStack {
                TextField("Aa", text: $value)
                    .modifier(CustomTextField())
                Button(action: {
                    print("comment sendt")
                }) {
                    Image(systemName: "location.fill")
                        .font(.title)
                        .foregroundColor(Color("Text"))
                }
            }
            .padding(15)
            .background(Color(.systemBackground))
    }
}

struct CommentInputView_Previews: PreviewProvider {
    static var previews: some View {
        CommentInputView().previewLayout(.sizeThatFits)
    }
}
