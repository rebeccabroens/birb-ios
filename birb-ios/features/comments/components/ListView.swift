//
//  ListView.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import SwiftUI

struct ListView: View {
    @ObservedObject var commentStore = CommentStore()
    
    var body: some View {
        VStack {
            List(commentStore.comments) { comment in
                ListItemView(comment: comment)
                    .listRowBackground(Color.white.opacity(0))
                    .listRowInsets(EdgeInsets(top: 0, leading: 8, bottom: 12, trailing: 8))
            }
            .edgesIgnoringSafeArea(.all)
            .onAppear() {
                self.commentStore.fetchLatestComments()
                UITableView.appearance().separatorStyle = .none
                UITableViewCell.appearance().backgroundColor = .none
                UITableView.appearance().backgroundColor = .none
            }
            .onDisappear() {
                UITableView.appearance().separatorStyle = .singleLine
                UITableViewCell.appearance().backgroundColor = .white
                UITableView.appearance().backgroundColor = .white
            }
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
