//
//  Comment.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import Foundation

struct Comment: Identifiable, Hashable {
    var id = UUID()
    var displayName: String
    var value: String
    
    init (displayName: String, value: String) {
        self.displayName = displayName
        self.value = value
    }
}

extension Comment: Codable {
    enum CodingKeys: String, CodingKey {
        case displayName
        case value
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        displayName = try values.decode(String.self, forKey: .displayName)
        value = try values.decode(String.self, forKey: .value)
    }
}
