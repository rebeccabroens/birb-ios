//
//  CommentStore.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import Foundation

class CommentStore: Decodable, ObservableObject{
    @Published var comments: [Comment] = []
    private static var url = "" //TO DO: endpoint url
    
    enum CodingKeys: CodingKey {
        case comments
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        comments = try values.decode([Comment].self, forKey: .comments)
    }
    
    init() {}
    
    func fetchLatestComments() {
        // TO DO
        
        self.comments = [
            Comment(displayName: "Gerald Johnson", value: "A nice cup of tea after a busy afternoon."),
            Comment(displayName: "Anne Addison", value: "I love talking a long hot relaxing bath."),
            Comment(displayName: "Bowie", value: "Snuggling up inside of a sleeve."),
            Comment(displayName: "Teddy", value: "I like to take my time and do something for myself!")
        ]
    }
}
