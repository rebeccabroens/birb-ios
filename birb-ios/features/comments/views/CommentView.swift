//
//  CommentView.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import SwiftUI

struct CommentView: View {
    
    @Binding var isShowing: Bool
    
    @State private var curHeight: CGFloat = 400
    let minHeight: CGFloat = 400
    let maxHeight: CGFloat = 700
    
    var body: some View {
        ZStack(alignment: .bottom) {
            if isShowing {
                Color.black
                    .opacity(0.3)
                    .ignoresSafeArea()
                    .onTapGesture {
                        isShowing = false
                    }
                mainView
                .transition(.move(edge: .bottom))
                
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
        .ignoresSafeArea()
    }
    
    var mainView: some View {
        VStack {
            Text("Comment")
                .font(.system(.body))
                .bold()
            ListView()
            CommentInputView()
                .padding(.bottom, 58.0)
        }
        .frame(height: 400)
        .frame(maxWidth: .infinity)
        .modifier(FlatGlassView())
    }
}


struct CommentView_Previews: PreviewProvider {
    static var previews: some View {
        CommentView(isShowing: .constant(true))
    }
}
