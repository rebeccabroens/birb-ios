//
//  FriendListItem.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 10/12/2021.
//

import SwiftUI

struct FriendListItemView: View {
    var friend: Friend
    var statusIcon: String {
        switch(friend.status) {
        case "Online":
            return "sun.max"
        case "Away":
            return "moon"
        default:
            return ""
        }
    }
    
    
    var body: some View {
        HStack {
            AsyncImage(url: URL(string: friend.image)) { image in
                image.resizable()
            } placeholder: {
                ProgressView()
            }
            .frame(width: 60, height: 60)
            .cornerRadius(15)
            VStack(alignment: .leading) {
                Text(friend.displayName)
                    .font(.system(.title2).bold())
                HStack {
                    Image(systemName: statusIcon)
                    Text(friend.status)
                        .font(.system(.subheadline))
                }
            }
            Spacer()
        }
        .padding(.all, 5)
    }
}

struct FriendListItem_Previews: PreviewProvider {
    static var previews: some View {
        FriendListItemView(friend: Friend(displayName: "Gerald Johnson", status: "Offline", image: "https://cdn.shopify.com/s/files/1/2233/5237/products/cnr-gcca_1024x1024@2x.jpg?v=1568821004")).previewLayout(.sizeThatFits)
    }
}
