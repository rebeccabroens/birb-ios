//
//  Friend.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 10/12/2021.
//

import Foundation

struct Friend: Identifiable, Hashable {
    var id = UUID()
    var displayName: String
    var status: String
    var image: String
    
    init(displayName: String, status: String, image: String) {
        self.displayName = displayName
        self.status = status
        self.image = image
    }
}

extension Friend: Codable {
    enum CodingKeys: String, CodingKey {
        case displayName = "displayname"
        case status
        case image
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        displayName = try values.decode(String.self, forKey: .displayName)
        status = try values.decode(String.self, forKey: .status)
        image = try values.decode(String.self, forKey: .image)
        
    }
}
