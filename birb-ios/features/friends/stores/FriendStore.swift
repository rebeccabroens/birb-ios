//
//  FriendStore.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 10/12/2021.
//

import Foundation

class FriendStore: Decodable, ObservableObject {
    @Published var friends: [Friend] = []
    private static var url = "https://birb-api.herokuapp.com/profile/id/friends" //TODO: use correct userid
    
    enum CodingKeys: CodingKey {
        case friends
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        friends = try values.decode([Friend].self, forKey: .friends)
    }
    
    init() { }
    
    func fetchFriends() {
        guard let friendUrl = URL(string: Self.url) else {
            return
        }
        
        let request = URLRequest(url: friendUrl)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
            if let error = error {
                print(error)
                return
            }
            if let data = data {
                DispatchQueue.main.async {
                    self.friends = self.parseJsonData(data: data)
                }
            }
        })
        task.resume()
    }
    
    func parseJsonData(data: Data) -> [Friend] {
        let decoder = JSONDecoder()
        
        do {
            let friendData = try decoder.decode([Friend].self, from: data)
            self.friends = friendData
        } catch {
            print(error)
        }
        
        return friends
    }
}
