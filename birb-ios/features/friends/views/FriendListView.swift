//
//  FriendListView.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 10/12/2021.
//

import SwiftUI

struct FriendListView: View {
    
    @EnvironmentObject var model: AppStateModel
    @ObservedObject var friendStore = FriendStore()
    @State private var searchText = ""
    
    var body: some View {
        VStack {
            List {
                ForEach(searchText == "" ? friendStore.friends : friendStore.friends.filter { $0.displayName.contains(searchText) }, id: \.self) { friend in
                    FriendListItemView(friend: friend)
                }
                .searchable(text: $searchText)
            }
            .listStyle(PlainListStyle())
        }
        .onAppear() {
            self.friendStore.fetchFriends()
        }
        .navigationTitle("Friends")
        .toolbar {
            ToolbarItem(placement: ToolbarItemPlacement.navigationBarLeading){
                Button("Sign Out") {
                    self.signOut()
                }
            }
            
        }
    }
    
    func signOut() {
        model.signOut()
    }
}

struct FriendListView_Previews: PreviewProvider {
    static var previews: some View {
        FriendListView()
    }
}
