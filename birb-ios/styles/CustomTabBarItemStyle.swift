//
//  CustomTabBarItemStyle.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import SwiftUI
import TabBar

struct CustomTabItemStyle: TabItemStyle {
    
    public func tabItem(icon: String, title: String, isSelected: Bool) -> some View {
        ZStack {
            Image(systemName: icon)
                .foregroundColor(isSelected ? .black : .gray)
                .frame(width: 32.0, height: 42.0)
        }
        .padding(.vertical, 8.0)
    }
}
