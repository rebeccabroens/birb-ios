//
//  CustomTextField.swift
//  birb-ios
//
//  Created by Michelle Broens on 06/01/2022.
//

import SwiftUI

struct CustomTextField: ViewModifier {
    func body(content: Content) -> some View {
        return content
            .padding(10)
            .foregroundColor(Color("Text"))
            .background(Color(.secondarySystemBackground))
            .cornerRadius(7)
    }
}
