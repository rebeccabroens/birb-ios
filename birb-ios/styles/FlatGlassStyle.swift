//
//  FlatGlassStyle.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import SwiftUI

struct FlatGlassView : ViewModifier {
    func body(content: Content) -> some View {
        if #available(iOS 15.0, *) {
            content
                .padding(.top, 20)
                .background(.thinMaterial)
                .cornerRadius(14)
        } else {
            // Fallback on earlier versions
            content
                .padding(.top, 20)
                .background(.white)
                .cornerRadius(14)
        }
    }
}
